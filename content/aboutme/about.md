---
title: About Me
# subtitle: Why you'd want to hang out with me
comments: false
---

My name is Luhan Cheng, a forth year computer science student. I'm interested in following topices which i would like to discuss in this blog: 

- scientific computing
- machine learning
- game theory
- reinforcement learning

--- 

## why blog?

> Teaching makes study more effective

Many past studies have shown that people can gain much deeper understanding of the materials through teaching others. I recently started my journey towards a research degree, which implies much of tutorials/lectures/readings to go through before getting started. So this is the place i'm going to experiment the **participatory teaching** !

![Learning-Pyramid-synap-2.png](../assets/img/Learning-Pyramid-synap-2.png)
